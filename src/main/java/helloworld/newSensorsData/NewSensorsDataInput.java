package helloworld.newSensorsData;

public class NewSensorsDataInput {
    private double temperature;
    private double humidity;
    private double illuminate;
    private double pressure;
    private double co2;
    private double ext_humidity;
    private double ext_temperature;

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public double getIlluminate() {
        return illuminate;
    }

    public void setIlluminate(double illuminate) {
        this.illuminate = illuminate;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public double getCo2() {
        return co2;
    }

    public void setCo2(double co2) {
        this.co2 = co2;
    }

    public double getExt_humidity() {
        return ext_humidity;
    }

    public void setExt_humidity(double ext_humidity) {
        this.ext_humidity = ext_humidity;
    }

    public double getExt_temperature() {
        return ext_temperature;
    }

    public void setExt_temperature(double ext_temperature) {
        this.ext_temperature = ext_temperature;
    }
}
