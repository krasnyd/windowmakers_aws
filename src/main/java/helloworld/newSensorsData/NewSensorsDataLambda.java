package helloworld.newSensorsData;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import helloworld.utils.EmptyClass;

public class NewSensorsDataLambda implements RequestHandler<NewSensorsDataInput, EmptyClass> {

    private DynamoDB dynamoDb;
    private String DYNAMODB_TABLE_NAME = "window_state";
    private Region REGION = RegionUtils.getRegion("us-east-2");

    public EmptyClass handleRequest(
            NewSensorsDataInput personRequest, Context context) {

        this.initDynamoDbClient();

        persistData(personRequest);

        EmptyClass emptyClass = new EmptyClass();

        return emptyClass;
    }

    private void persistData(NewSensorsDataInput pr)
            throws ConditionalCheckFailedException {

        System.out.println(pr.getTemperature());
        System.out.println(pr.getHumidity());
        System.out.println(pr.getIlluminate());
        System.out.println(pr.getPressure());
        System.out.println(pr.getCo2());
        System.out.println(pr.getExt_humidity());
        System.out.println(pr.getExt_temperature());

        UpdateItemSpec up = new UpdateItemSpec().withPrimaryKey("nothing", "now")
                .withUpdateExpression("set co2 = :co2, humidity = :humidity, pressure = :pressure, temperature = :temperature")
                .withValueMap(new ValueMap()
                        .withNumber(":co2", pr.getCo2())
                        .withNumber(":humidity", pr.getHumidity())
                        .withNumber(":pressure", pr.getPressure())
                        .withNumber(":temperature", pr.getTemperature())
                );
        this.dynamoDb.getTable("sensors_data").updateItem(up);

        Item i = this.dynamoDb.getTable(DYNAMODB_TABLE_NAME).getItem(new PrimaryKey("nothing", "now"));
        boolean actualState = (boolean) i.get("opened");
        System.out.println("State: " + actualState);

        if(pr.getCo2() > 1000){
            setOpened(true);
        } else if(pr.getTemperature() > 34){
            setOpened(true);
        } else if(actualState && pr.getTemperature() < 32) {
            setOpened(false);
        }

    }

    private void setOpened(boolean setTo){
        System.out.println("changing state to: "+setTo);
        UpdateItemSpec up = new UpdateItemSpec().withPrimaryKey("nothing", "now")
                .withUpdateExpression("set opened = :val")
                .withValueMap(new ValueMap().withBoolean(":val", setTo));
        this.dynamoDb.getTable(DYNAMODB_TABLE_NAME).updateItem(up);
    }

    private void initDynamoDbClient() {
        AmazonDynamoDBClient client = new AmazonDynamoDBClient();
        client.setRegion(REGION);
        this.dynamoDb = new DynamoDB(client);
    }

}