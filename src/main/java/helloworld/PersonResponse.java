package helloworld;

public class PersonResponse {
    private boolean state;

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    // standard getters and setters
}