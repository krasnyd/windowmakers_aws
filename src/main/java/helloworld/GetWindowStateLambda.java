package helloworld;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import helloworld.utils.EmptyClass;

public class GetWindowStateLambda implements RequestHandler<EmptyClass, PersonResponse> {

    private DynamoDB dynamoDb;
    private String DYNAMODB_TABLE_NAME = "window_state";
    private Region REGION = RegionUtils.getRegion("us-east-2");

    public PersonResponse handleRequest(
            EmptyClass emptyClass, Context context) {

        this.initDynamoDbClient();

        boolean be = persistData(emptyClass);

        PersonResponse personResponse = new PersonResponse();
        personResponse.setState(be);
        return personResponse;
    }

    private boolean persistData(EmptyClass emptyClass)
            throws ConditionalCheckFailedException {
        Item i = this.dynamoDb.getTable(DYNAMODB_TABLE_NAME).getItem(new PrimaryKey("nothing", "now"));
        System.out.println("State: " + i.get("opened"));
        return (boolean) i.get("opened");
    }

    private void initDynamoDbClient() {
        AmazonDynamoDBClient client = new AmazonDynamoDBClient();
        client.setRegion(REGION);
        this.dynamoDb = new DynamoDB(client);
    }

}