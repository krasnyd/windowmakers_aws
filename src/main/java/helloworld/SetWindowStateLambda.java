package helloworld;


import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class SetWindowStateLambda implements RequestHandler<PersonSet, PersonResponse> {

    private DynamoDB dynamoDb;
    private String DYNAMODB_TABLE_NAME = "window_state";
    private Region REGION = RegionUtils.getRegion("us-east-2");

    public PersonResponse handleRequest(
            PersonSet personRequest, Context context) {

        this.initDynamoDbClient();
        persistData(personRequest);

        PersonResponse personResponse = new PersonResponse();

        return personResponse;
    }

    private void persistData(PersonSet personRequest)
            throws ConditionalCheckFailedException {
        System.out.println(personRequest.getOpened());
        UpdateItemSpec up = new UpdateItemSpec().withPrimaryKey("nothing", "now")
                .withUpdateExpression("set opened = :val")
                .withValueMap(new ValueMap().withBoolean(":val", personRequest.getOpened()));
        this.dynamoDb.getTable(DYNAMODB_TABLE_NAME).updateItem(up);

//        Item i = this.dynamoDb.getTable(DYNAMODB_TABLE_NAME).getItem(new PrimaryKey("nothing", "now"));
//        System.out.println("State: " + i.get("opened"));
    }

    private void initDynamoDbClient() {
        AmazonDynamoDBClient client = new AmazonDynamoDBClient();
        client.setRegion(REGION);
        this.dynamoDb = new DynamoDB(client);
    }


}