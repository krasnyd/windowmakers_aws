package helloworld.getSensorsData;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import helloworld.PersonResponse;
import helloworld.utils.EmptyClass;

public class GetSensorsDataLambda implements RequestHandler<EmptyClass, GetSensorsDataOutput> {

    private DynamoDB dynamoDb;
    private String DYNAMODB_TABLE_NAME = "sensors_data";
    private Region REGION = RegionUtils.getRegion("us-east-2");

    public GetSensorsDataOutput handleRequest(
            EmptyClass emptyClass, Context context) {

        this.initDynamoDbClient();




        return persistData(emptyClass);
    }

    private GetSensorsDataOutput persistData(EmptyClass emptyClass)
            throws ConditionalCheckFailedException {
        GetSensorsDataOutput gsda = new GetSensorsDataOutput();
        Item i = this.dynamoDb.getTable(DYNAMODB_TABLE_NAME).getItem(new PrimaryKey("nothing", "now"));
        gsda.setCo2(i.getNumber("co2").doubleValue());
        gsda.setHumidity(i.getNumber("humidity").doubleValue());
        gsda.setPressure(i.getNumber("pressure").doubleValue());
        gsda.setTemperature(i.getNumber("temperature").doubleValue());
        System.out.println(gsda);

        return gsda;
    }

    private void initDynamoDbClient() {
        AmazonDynamoDBClient client = new AmazonDynamoDBClient();
        client.setRegion(REGION);
        this.dynamoDb = new DynamoDB(client);
    }

}
